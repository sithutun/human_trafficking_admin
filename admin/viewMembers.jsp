<%@ page contentType="text/html; charset=UTF-8"%>
<%-- <%@ page language="java" contentType="text/html; charset=unicode"  %> --%>
<%@ taglib prefix="s" uri="/struts-tags" %> 


<head>
<%--  <link href="<%=request.getContextPath() %>/css/demo_page.css" rel="stylesheet" type="text/css" />  --%>
<link href="<%=request.getContextPath()%>/css/demo_table.css" rel="stylesheet" type="text/css" />      
<link href="<%=request.getContextPath() %>/css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
	<!-- Scripts -->
<script src="<%=request.getContextPath() %>/js/jquery.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/jquery.dataTables.js" type="text/javascript"></script>
	<script type="text/javascript">
	
	 $(document).ready(function () {
            $("#members").dataTable({
              "sPaginationType": "full_numbers",
               "bJQueryUI": true              
           });
       });
	 </script>
	 <style type="text/css">
#informer a{
font-size: 20px;
line-height: 90px;
width: 200px;
margin:5px;
padding:10px;
background-color: #eafo22;
border-radius:6px;
box-shadow:2px 3px 6px black;
text-decoration: none;
}
#caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-top:5px;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}

</style>
</head>
<body id="dt_example">
<div class="sketch">
	<div id="container" class="pra"><header>
	<h1></h1>
	<div id="demo_jui">
	<s:form action="manageMemberAction"  theme="simple">
		<table id="sales" class="display" >
		<span id="caption" ><center>အဖွဲ့ဝင်များ</center></span>
  		 <tr><td></td><td>ပြည်နယ်/တိုင်း
<s:select name="idString" list="divlist"  listKey="division_name" listValue="division_name"   ></s:select><s:submit value="ရှာပါ" method="searchMemberByDiv" ></s:submit></td><td>
</td></tr></table>
</s:form>

<s:url action="insertMember"  method="selectDivision" var="insert">	</s:url> 
			<s:a	href="%{insert}" >အဖွဲ့ဝင်အသစ်ထည့်</s:a> 
			
	<table id="members" class="display">
	
              <thead>
	                  <tr>
                    	 <th>အဖွဲ့ဝင်အမည်</th>
                     	<th>ရာထူး</th>
                     	<th></th>
	                  </tr>
	              </thead>
              <tbody>
       <s:iterator value="memberList">
      	<tr>
          	<td><s:property value="member_name"/></td>
	        <td><s:property value="position"/></td>
	        <td> <s:url action="memberDetailAction"  method="memberDetail"  var="md">
				<s:param name="id"  value="member_id"></s:param>
			</s:url>
	         	<s:a href="%{md}">အသေးစိတ်အချက်အလက်</s:a></td>
      </tr>
    </s:iterator>   
           </tbody>
          </table>
          </div>
	 </div>
	 </div>
	 </body>
	 