<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
input[type="submit"]{
padding: 10px;

}
input[type="submit"]:hover{
background-color: #eef;
}
</style>
</head>
<body><s:form  action="sendMessage"   theme="simple">
<s:hidden name="instruction.c.case_id"  value="%{id}"></s:hidden>
<table>
<caption>မှတ်ချက်ရေးသွင်းရန်</caption>
<tr><td style="vertical-align:top;">မှတ်ချက်</td><td>
 <s:textarea  name="instruction.remark" label=""  cols="55" rows="10"></s:textarea></td></tr>
<tr><td></td><td><s:submit value="ပေးပို့မည်" method="send" ></s:submit></td></tr>
</table>

</s:form>
<s:url action="BackToCaseDetail" var="casedetail">

</s:url>
<s:a href="%{casedetail}"></s:a>

</body>
</html>