<%@ page contentType="text/html; charset=UTF-8"%>
<%-- <%@ page language="java" contentType="text/html; charset=unicode"  %> --%>
<%@ taglib prefix="s" uri="/struts-tags" %> 


<head>
<%--  <link href="<%=request.getContextPath() %>/css/demo_page.css" rel="stylesheet" type="text/css" />  --%>
<link href="<%=request.getContextPath()%>/css/demo_table.css" rel="stylesheet" type="text/css" />      
<link href="<%=request.getContextPath() %>/css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
	<!-- Scripts -->
<script src="<%=request.getContextPath() %>/js/jquery.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/js/jquery.dataTables.js" type="text/javascript"></script>
	<script type="text/javascript">
	
	 $(document).ready(function () {
            $("#cases").dataTable({
              "sPaginationType": "full_numbers",
               "bJQueryUI": true              
           });
       });
	 </script>
</head>
<body id="dt_example">
<!-- <div class="sketch"> -->
	<div id="container" class="pra">
	<h1></h1>
	<div id="demo_jui">
	<table id="cases" class="display" >
  		 <thead>
	                  <tr>
                    	 <th>ရက်စွဲ</th>
                     	<th>အမှုအမျိုးအစား</th>
                    	<th>ဦးတည်နိုင်ငံ</th>
	                    <th>မှုခင်းနေရာ</th>
	                     <th></th>
	                   
	                  </tr>
	              </thead>
              <tbody>
              
       <s:iterator value="caselist">
     
      	<tr style="background-color:#f5ed70 ;">
          	<td><s:property value="inform_date"/></td>
	        <td><s:property value="trafficking_type"/></td>
	        <td><s:property value="destination_country"/></td>
	        <td><s:property value="case_location"/></td>
	         <td>
	         	<s:url action="caseDetailAction" method="caseDetail" var="detailLink">
	         		<s:param  name="id" value="case_id"/>
	         	</s:url>
				<a href="${detailLink}">အသေးစိတ်အချက်အလက်</a><br>
	         </td>
</tr>
</s:iterator>   
           </tbody>
          </table>
          </div>
	 </div>
</body>
	</html>