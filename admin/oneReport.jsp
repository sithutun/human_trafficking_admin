
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<h2 align="center">အစီရင်ခံစာအပြည့်အစုံ</h2>
<s:form action="addPeopleAction"    theme="simple">
<s:param name="id"  value="report.report_id"></s:param>
<s:param name="tv.c.case_id" value="%{report.c.case_id}"/>
	
	<table width="80%" cellpadding="7px" >
		<tr>
			<th colspan="4">အမှုအသေးစိတ်အချက်အလက်</th>
		</tr>
		<tr>
			<td width="20px"></td>
			<td width="40%">အမှု အခြေအနေ</td>
			<td width="5px">-</td>
			<td align="left"><s:property value="report.c.status" /></td>
		</tr>
		<tr>
			<td></td>
			<td>ဖြစ်ပွားသည့်နေ့</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.inform_date" /></td>
		</tr>
		<tr>
			<td></td>
			<td>လူကုန်ကူးမှု အမျိုးအစား</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.trafficking_type" /></td>
		</tr>
		<tr>
			<td></td>
			<td>လူကုန်ကူးသူ အရေအတွက်</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.no_trafficker" /></td>
		</tr>
		<tr>
			<td></td>
			<td>လူကုန်ကူးခံရနိုင်မည့် အရေအတွက်</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.no_victim" /></td>
		</tr>
		<tr>
			<td></td>
			<td>တွေ့ရှိရသော လူများ</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.person_in_case" /></td>
		</tr>
		<tr>
			<td></td>
			<td>ဖြစ်ပွားရာမြို့</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.case_location" /></td>
		</tr>
		<tr>
			<td></td>
			<td>တိုင်းဒေသကြီး</td>
			<td>-</td>
			<td align="left"><s:property
					value="report.c.division.division_name" /></td>
		</tr>
		<tr>
			<td></td>
			<td>လူကုန်ကူးနိုင်မည့် နိုင်ငံ</td>
			<td>-</td>
			<td align="left"><s:property
					value="report.c.destination_country" /></td>
		</tr>
		<tr>
			<td></td>
			<td valign="top">အမှု ထင်မြင်ချက်</td>
			<td>-</td>
			<td align="left"><s:property value="report.c.description" /></td>
		</tr>
		<tr>
			<td></td>
			<td valign="top">အမှုဖြည့်စွက်အချက်အလက်များ</td>
			<td>-</td>
			<td><s:property value="report.c.detail_description" /></td>
		</tr>
	</table>
	<br>
	
	
	<table cellpadding="7px" >
		<tr>
			<th colspan="4">အစီရင်ခံချက်များ</th>
		</tr>
		<tr>
			<td width="20px"></td>
			<td width="20%">ပေးပို့သည့်ရက်</td>
			<td width="5px">-</td>
			<td align="left"><s:property value="report.reportDate" /></td>
		</tr>
		<tr>
			<td></td>
			<td>ပေးပို့သူ</td>
			<td>-</td>
			<td align="left"><s:property value="report.member.name" /></td>
		</tr>
		<tr>
			<td colspan="4"><br /></td>
		</tr>
		<tr>
			<td></td>
			<td valign="top">အကြောင်းအရာ</td>
			<td>-</td>
			<td align="left"><s:property value="report.title" /></td>
		</tr>
		<tr>
			<td></td>
			<td valign="top">ဖော်ပြချက်</td>
			<td>-</td>
			<td align="left"><s:property value="report.description" /></td>
		</tr>
		<tr><td></td><td>
		<input type="button"  value="အစီရင်ခံစာပြန်ပို့ရန်" onclick="location.href='<%= request.getContextPath() %>/reportAgainAction.action?id=<s:property value="report.report_id"/>'"></button>
		</td><td>
		<input type="button"  value="မှတ်တမ်းတင်ရန်" onclick="location.href='<%=request.getContextPath()%>/recordCaseAction.action?id=<s:property value="report.report_id"/>'"></button>
		</td><td></td></tr>
	</table>
	
	<br/><br/>
	

	<%-- <table align="left" cellpadding="7px" >
		<tr>
			<th colspan="5">အမှုနှင့်သက်ဆိုင်သောအချက်အလက်</th>
		<tr>
		<tr>
			<td align="right">အမည်</td>
			<td >-<s:textfield name="tv.rv_name" /></td>
			<td width="15px"></td>
			<td  align="right">အခြားအမည်</td>
			<td >-<s:textfield name="tv.nickname" /></td>
		</tr>
		<tr>
			<td  align="right">မှတ်ပုံတင်အမှတ်</td>
			<td>-<s:textfield name="tv.pdetail.nrc" />
			<td width="15px"></td>
			<td  align="right">ဤသူသည်</td>
			<td>-<s:select name="tv.type" list="{'ဒုက္ခသည်','လူကုန်ကူးသူ'}"  /></td>
		</tr>
		<tr>
			<td  align="right">အဖ အမည်</td>
			<td>-<s:textfield name="tv.pdetail.father_name" /></td>
			<td width="15px"></td>
			<td  align="right">ကျား/မ</td>
			<td>-<s:select name="tv.pdetail.gender" list="{'မ','ကျား'}" /></td>
		<tr>
			<td  align="right">မွေးသက္ကရာဇ်</td>
			<td>-<s:textfield name="tv.pdetail.dob" /></td>
			<td width="15px"></td>
			<td  align="right"><!-- ဓာတ်ပုံ --></td>
			 <td>-<s:file name="tv.pdetail.photo" /></td> 
		</tr>

	</table>
  --%>
 	<%-- <br>
	<div align="center">
		<s:submit value="သိမ်းမည်" method="insertPeople" />		
		<s:submit value="ပြီးပြီ" />
		</div> --%>
</s:form>
