<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<style type="text/css">
</style>
<body style="background-color: silver">
	<div align="center" style="background: -moz-linear-gradiend(top, gray 30%, blue 100%); margin-top: 100px; margin-left: 200px; width: 400px; height: 300px; background-color: white; box-shadow: 5px 5px 5px 5px gray">
		<div style="background: #008080; height:50px">
			<h2 style="color: #F3A300;padding-top:5px;">
				<s:text name="စနစ်သို့ ဝင်ရန်" />
			</h2>
		</div>
		<s:form action="verify"  theme="simple">
			<table cellpadding="5px" >
				<tr>
					<td colspan="2">&nbsp;<s:fielderror fieldName="admin.name"></s:fielderror></td>
				</tr>
				<tr>
					<td>အမည်</td>
					<td><s:textfield name="admin.name" /></td>
				</tr>
				<tr>
					<td>လျှို့ဝှက် နံပါတ်</td>
					<td><s:password name="admin.password" /></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;<s:submit value="စနစ်သို့ ဝင်ရန်" /></td>
				</tr>
			</table>
		</s:form>
<br/>
	</div>