<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>

<style type="text/css">
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
input[type="submit"]{
padding: 10px;

}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
nav a{
text-decoration: none;
line-height: 45px;
padding: 10px;
width:100px;
font-size: 20px;
box-shadow:2px 3px 4px black;
}
nav a:hover{
text-decoration: none;}
</style>
</head>
<body >
<s:form action="insertMemberSuccess"  enctype="multipart/form-data"  theme="simple">
	<table >
	<caption>အဖွဲ့ဝင်အသစ်ခန့်အပ်ခြင်း</caption>
	
	<tr><td><img alt="ဓာတ်ပုံ" src="<s:property value="member.personal_Detail.photo" />"  width="100px"  height="160px">	</td><td></td></tr>
	 <tr><td></td><td> <s:actionerror/> </td><td></td></tr>
	 <tr><td><s:file name="upload.image" ></s:file> </td><td></td><td></td></tr>
	  <tr><td></td><td> <s:fielderror fieldName="member.member_name" /> </td><td></td></tr>
	<tr>
			<td>အဖွဲ့ဝင်နာမည်</td>
			<td><s:textfield name="member.member_name"/></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.password" /> </td><td></td></tr>
		<tr>
		
			<td>လျို့ဝှက်စာသား</td>
			<td><s:password name="member.password"   /></td>
			<td></td>
		</tr>
		
			<tr>
			<td>လျို့ဝှက်စာသားပြန်ရိုက်ရန်</td>
			<td><s:password name="member.retype_password"   /></td>
			<td></td>
		</tr> 
	<tr><td></td><td> <s:fielderror fieldName="member.name" /> </td><td></td></tr>
		<tr>
			<td>နာမည်အရင်း</td>
			<td><s:textfield name="member.name" key="နာမည်အရင်း"  /></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.personal_Detail.father_name" /> </td><td></td></tr>
				<tr>
			<td>အဖအမည်</td>
			<td><s:textfield name="member.personal_Detail.father_name" key="အဖအမည်" /></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.position" /> </td><td></td></tr>
		<tr>
			<td>ရာထူး</td>
			<td><s:textfield name="member.position" key="ရာထူး" /></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.personal_Detail.nrc" /> </td><td></td></tr>
		<tr>
			<td>မှတ်ပုံတင်</td>
			<td><s:textfield name="member.personal_Detail.nrc" key="မှတ်ပုံတင်" /></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.personal_Detail.dob" /> </td><td></td></tr>
		<tr><td>မွေးသက္ကရာဇ်</td>
			<td><s:textfield name="member.personal_Detail.dob" key="မွေးသက္ကရာဇ်" /></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.personal_Detail.address.phone" /> </td><td></td></tr>
		<tr>
			<td>ဖုန်း</td>
			<td><s:textfield name="member.personal_Detail.address.phone"/></td>
			<td></td>
		</tr>
		
		<tr>
			<td>ကျား/မ</td>
			<td><s:radio name="member.personal_Detail.gender" key="ကျား/မ" list="{'ကျား','မ'}" /></td>
			<td></td>
		</tr>
		<tr>
			<td>တိုင်းနှင့်ပြည်နယ်</td>
			<td><s:select name="member.division.division_name"  list="divlist"  listKey="division_name" listValue="division_name" /></td>
			<td></td>
		</tr>
		<tr><td></td><td> <s:fielderror fieldName="member.personal_Detail.address" /> </td><td></td></tr>
		<tr>
			<td colspan="2">လိပ်စာ</td><td></td></tr>
		<tr><td>
			<label>အိမ်အမှတ်</label></td><td><s:textfield name="member.personal_Detail.address.home_no"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>လမ်း</label></td><td><s:textfield name="member.personal_Detail.address.street"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>ရပ်ကွက်</label></td><td><s:textfield name="member.personal_Detail.address.quarter"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>မြို့</label></td><td><s:textfield name="member.personal_Detail.address.town"></s:textfield>
			</td><td></td></tr>
		<tr><td>	<s:url action="manageMemberAction"  var="mm"></s:url> 
		<nav><s:a href="%{mm}">နောက်သို့</s:a></nav></td>
			<td><s:submit key="အသစ်ထည့်မည်" align="center"/></td>
		</tr>
	</table>
	</s:form>
</body>
</html>