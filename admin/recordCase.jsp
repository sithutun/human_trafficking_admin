<%@ page contentType="text/html; charset=UTF-8"%>
    <%@ taglib uri="/struts-tags"  prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<h3 align="center">အမှုနှင့်သက်ဆိုင်သူများမှတ်တမ်းတင်ခြင်း</h3><br><br>
<s:form action="recordCaseAction" theme="simple" >
<s:hidden name="id"  value="%{report.report_id}"></s:hidden>
<s:hidden name="tv.c.case_id" value="%{report.c.case_id}"/>
 <table align="left" cellpadding="7px" >
 	<tr>
	
			<td valign="top" >အကြောင်းအရာ</td>
			<td>-</td>
			<td align="left"><s:property value="report.title" /></td>		<td></td><td></td>
		</tr>
		<tr>
		
			<td valign="top">ဖော်ပြချက်</td>
		
			<td>-</td>
			<td align="left"><s:property value="report.description" /></td>	<td></td>	<td></td>
		</tr>
		<tr>
			<th colspan="5">အမှုနှင့်သက်ဆိုင်သောအချက်အလက်</th>
		</tr>
		<tr>
			<td align="right">အမည်</td>
			<td ><s:textfield name="tv.rv_name" /></td>
			<td width="15px"></td>
			<td  align="right">အခြားအမည်</td>
			<td ><s:textfield name="tv.nickname" /></td>
		</tr>
		<tr>
			<td  align="right">မှတ်ပုံတင်အမှတ်</td>
			<td><s:textfield name="tv.pdetail.nrc" />
			<td width="15px"></td>
			<td  align="right">ဤသူသည်</td>
			<td><s:select name="tv.type" list="{'ဒုက္ခသည်','လူကုန်ကူးသူ'}"  /></td>
		</tr>
		<tr>
			<td  align="right">အဖ အမည်</td>
			<td><s:textfield name="tv.pdetail.father_name" /></td>
			<td width="15px"></td>
			<td  align="right">ကျား/မ</td>
			<td><s:select name="tv.pdetail.gender" list="{'မ','ကျား'}" /></td>
		<tr>
			<td  align="right">မွေးသက္ကရာဇ်</td>
			<td><s:textfield name="tv.pdetail.dob" /></td>
			<td width="15px"></td>
			<td  align="right"><!-- ဓာတ်ပုံ --></td>
			 <td><%-- <s:file name="tv.pdetail.photo" /> --%></td> 
		</tr>
		<tr><td colspan="5"> &nbsp;</td></tr>
		<tr><td colspan="5"> &nbsp;</td></tr>
	<tr><td colspan="5" align="center"><s:submit value="သိမ်းမည်" />		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<s:submit value="ပြီးပြီ" method="" /> </td></tr>
	</table>
  <br>
	<%-- <div align="center">
	<input type="button"  value="ပြီးပြီး" onclick="location.href='<%=request.getContextPath() %>/caseDetailAction.action!caseDetail?id=<s:property value="tv.c.case_id"/>'"/>
		<s:submit value="သိမ်းမည်" />		
		<s:submit value="ပြီးပြီ" method="" />
		</div> --%>
		</s:form>
</body>
</html>