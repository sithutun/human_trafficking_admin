<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<style>
#frame {
		background-color: skyblue;
		width: 80%;
		padding-left: 10%;
		padding-top: 10%;
		padding: 20px;
		margin-top: 10%;
		box-shadow: 2px 2px 2px 2px #lightgreen;
	}

table {
		padding: 20px;
		width: 80%;
	}
	
<%int i=0;%>
<%if ( i % 2 == 0){%>
<tr style ="background-color: #F8F8F8 ;">
<%}else if (i % 2 != 0){%>
<tr style ="background-color : #D4D4D4;"> <%
}i++;%> </style>
<%--  <link href="<%=request.getContextPath() %>/css/demo_page.css" rel="stylesheet" type="text/css" />  --%>
<link href="<%=request.getContextPath()%>/css/demo_table.css"
	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/demo_table_jui.css"
	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/jquery-ui.css"
	rel="stylesheet" type="text/css" media="all" />
<!-- Scripts -->
<script src="<%=request.getContextPath()%>/js/jquery.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#sales").dataTable({
			"sPaginationType" : "full_numbers",
			"bJQueryUI" : true
		});
	});
</script>
</head>
<body id="dt_example">

<h1></h1>
<h3 align="center">အစီရင်ခံစာများ</h3>
<s:form action="viewDivisionReportAction" theme="simple">
	<s:select name="divname" list="divlist" listKey="division_name"
		listValue="division_name" />
	<s:submit value="ရှာပါ"></s:submit>
</s:form>
	<div id="container" class="pra"><header>
		<h1></h1>
		<div id="demo_jui">
		<s:if test="%{reportlist.size!=0}">
			<table id="sales" class="display">


				<thead>
					<tr bgcolor="#B4CDE6" align="center">
						<th><div align="center">အကြောင်းအရာ</div></th>
						<th><div align="center">ပေးပို့သူ</div></th>
						<th><div align="center">ပေးပို့သည့်ရက်</div></th>
						<th><div align="center">အခြေအနေ</div></th>
						<th><div align="center">လုပ်ဆောင်ရန်</div></th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="reportlist">
						<tr>
							<td><s:property value="title" /></td>
							<td><s:property value="member.name" /></td>
							<td><s:property value="reportDate" /></td>
							<td><s:property value="c.status" /></td>
							<td><s:url action="viewReportDetailAction" var="link">
									<s:param name="reportId" value="report_id" />
							</s:url> <s:a href="%{link}">ကြည့်ရှုရန်</s:a></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			</s:if>
		</div></header>
	</div>
	
</body>





