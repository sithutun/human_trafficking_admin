<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#informer a{
font-size: 20px;
line-height: 90px;
width: 200px;
margin:5px;
padding:10px;
background-color: #eafo22;
border-radius:6px;
box-shadow:2px 3px 6px black;
text-decoration: none;
}
#informer a:hover{
background-color: #ea40ff;

}
input[type="text"]{
font-size:12px;
padding: 10px;
}
td{
font-size: 14px;
spacing:5px;
padding: 8px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-top:5px;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}

</style>
</head>
<body>

<div id="case">
<s:form action=""  theme="simple"  >
<s:url action="vdetailAction" var="vdetail"></s:url>
<s:url action="tdetailAction" var="tdetail" ></s:url>

<table cellpadding="2px">
<caption>အမှုအသေးစိတ်အချက်အလက်</caption>
		<tr><td>ရက်စွဲ</td><td><s:textfield readonly="true"  name="c.inform_date" >   </s:textfield></td> </tr>
		<tr>
			<td>လူကုန်ကူးမှုအမျိုးအစား</td><td><s:textfield  readonly="true"  name="c.trafficking_type" ></s:textfield></td>
			
		</tr>
		<tr>
			<td>လူကုန်ကူးမည့်နိုင်ငံ</td>
			<td><s:textfield readonly="true"  name="c.destination_country" ></s:textfield></td>
		</tr>	
		<tr>
			<td>အမှုဖြစ်ပွားရာမြို့</td>
			<td><s:textfield readonly="true" name="c.case_location" ></s:textfield></td>
		</tr>	
		<tr>
			<td>အမှုဖြစ်ပွားရာပြည်နယ်/တိုင်း</td>
			<td><s:textfield readonly="true" name="c.division.division_name" ></s:textfield></td>
		</tr>	
	<tr>
			<td>တွေ့ရှိရသည့်လူများ</td>
			<td><s:textfield readonly="true" name="c.person_in_case" ></s:textfield></td>
		</tr>	
		<tr><td>အမှုထင်မြင်ချက်</td><td><s:textarea readonly="true" cols="40" rows="5"  name="c.description"   ></s:textarea> </td></tr>
		<tr>
			<td>အသေးစိတ်အချက်အလက်</td>
			<td><s:textarea readonly="true" name="c.detail_description" cols="40" rows="5"  ></s:textarea></td>
		</tr>
		</table>
	</s:form>	 
</div>
                    	
<div id="informer">
<s:if test="%{c.info!= null}">
	<table>
	<caption>သတင်းပေးသူ</caption>
		<tr>
			<td>အမည်</td>
			<td><s:property value="c.info.name"/> </td>
		</tr>
		<tr>
			<td>မှတ်ပုံတင်</td>
			<td><s:property value="c.info.nrc" /></td>
		</tr>
		<tr>
			<td>အီမေးလ်</td>
			<td><s:property value="c.info.email" /> </td>
		</tr>
		<tr>
			<td>ကျား/မ</td>
			<td><s:property value="c.info.gender" /></td>
		</tr>
		<tr>
			<td>ဖုန်းနံပါတ်</td>
			<td><s:property value="c.info.phone_no" /></td>
		</tr>
		<tr>
			<td>လိပ်စာ</td>
			<td><s:property value="c.info.address"/></td>
		</tr>
	</table>
	</s:if>
	<s:url action="homeAction" method="retrieveCaseFile"  var="case"></s:url>
	<s:a href="%{case}">နောက်သို့</s:a>
	<s:url action="Members"  method="displayID"  var="members" >
		<s:param name="id" value="c.case_id"></s:param>
		<s:param name="idString" value="c.division.division_name"></s:param>
	</s:url><s:a href="%{members}" >ပေးပို့မည်</s:a> 
	
	</div>
</body>
</html>