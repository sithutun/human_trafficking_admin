<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
	function submitForm(form, location) {
		var check = confirm('ဖျက်ရန် သေချာပါသလား');
		if (check) {
			form.submit();
			return true;
		}
		return false;
	}
</script>
<style type="text/css">
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
input[type="submit"]{
padding: 10px;

}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
nav a{
text-decoration: none;
line-height: 55px;
padding: 10px;
width:100px;
font-size: 20px;
box-shadow:2px 3px 4px black;
}
nav a:hover{
text-decoration: none;}
</style>
<script type="text/javascript">
function confirmDelete(form){
	var button=confirm("တကယ်ဖြတ်မှာလား");
 	if(buuton)
 		{
 		form.submit();
 		}
}
</script>
</head>
<body><form>
<table>
<caption>အဖွဲ့ဝင်၏အသေးစိတ်အခြေအနေ</caption>
<tr><td colspan="2"><img alt="ဓာတ်ပုံ" src="<s:property value="member.personal_Detail.photo"/>" width="100px" height="160px"> </td></tr>
	<tr>
<tr><td>အဖွဲ့ဝင်နာမည်</td><td><s:property  value="member.member_name" /></td></tr>
<tr><td>လျို့ဝှက်စာသား</td><td><s:property  value="member.password" /></td></tr>
<tr><td>အမည်</td><td><s:property value="member.name"/> </td></tr>
<tr><td>အဖအမည်</td><td><s:property value="member.personal_Detail.father_name"/> </td></tr>
<tr><td>ရာထူး</td><td> <s:property value="member.position"/> </td></tr>
<tr> <td>ကျား/မ</td><td> <s:property value="member.personal_Detail.gender"/> </td> </tr>
<tr> <td>မွေးသက္ကရာဇ်</td><td> <s:property value="member.personal_Detail.dob" /> </td> </tr>
<tr> <td>မှတ်ပုံတင်</td><td> <s:property value="member.personal_Detail.nrc" /> </td> </tr>
<tr><td>လိပ်စာ</td><td><s:property value="member.personal_Detail.address.maddress"/> </td></tr>
<tr><td>ဖုန်းနံပါတ်</td><td> <s:property value="member.personal_Detail.address.phone"/> </td></tr>

</table><nav>
<s:url action="manageMemberAction"  method="selectDivision"  var="back"></s:url>
<s:a href="%{back}">ရှေ့သို့</s:a>
<s:url action="updateMember" method="memberDetail" var="um">
<s:param name="id" value="member.member_id"></s:param>
</s:url>

<s:a href="%{um}">ပြင်ဆင်မည်</s:a>
<s:url action="deleteMemberAction"  method="delete" var="dm" >
	<s:param name="id" value="member.member_id"></s:param>
</s:url>
<s:a href="%{dm}"  onclick="submitForm(this.form,'deleteMemberAction')" >ပယ်ဖျက်မည်(ထုတ်ပယ်မည်)</s:a>
 <!-- <input type="button" onclick="submitForm(this.form,'deleteMemberAction')"value="ပယ်ဖျက်မည်(ထုတ်ပယ်မည်)" />  -->
</nav>
</form>
</body>
</html>