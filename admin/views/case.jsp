 <%@ page contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Case</title>
</head>
<body  bgcolor="pink">
<h1 align="center">လူကုန်ကူးမူ အမူတွဲ </h1>
<s:form action="caseAction"  theme="simple">

<table align="center" >
	<tr>
		<td>အမူတွဲ အမှတ်</td>
	    <td><s:textfield name="caseid"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>အမူတွဲ အမည်</td>
	    <td><s:textfield name="casename"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>ရက်စွဲ</td>
	    <td><s:textfield name="casedate"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>လူကုန်ကူးမူ အမျိုးအစား</td>
	    <td><s:textfield name="trafficktype"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	
	<tr>
		<td>ဖြစ်ပွားသည့်နေရာ</td>
	    <td><s:textfield name="caselocation"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>ဆိုက်ရောက်သည့်နိုင်ငံ</td>
	    <td><s:textfield name="destinCountry"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>ပြည်နယ်/တိုင်း</td>
	    <td><s:textfield name="divid"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	
	<tr>
		<td>အကြောင်းအရာ</td>
		<td><s:textarea cols="50" rows="10"  name="casearea" /></td>
		<td></td>
	</tr>
	<tr>
	<td></td>
	<td><s:submit value=" အမူတွဲ သိမ်းရန်"  align="center"/></td> 
	</tr>
	<tr>
	<td></td>
	<td><s:submit value="မလိုအပ်ပါက ပယ်ဖျက်ရန်"  align="center"/></td> 
	</tr>
	
</table>
<%-- <div align="center">
		<s:submit key="label.submit" align="center"/>
</div> --%>
</s:form>
</body>
</html>