<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>
<body>
<table width="100%">
			<tr>
			<td>
					<s:url action="updateMember" var="update"></s:url>
				<s:a href="%{update}">ပြင်ဆင်ရန်</s:a> 
				<s:url action="deleteMember" var="delete"></s:url>
				<s:a href="%{delete}">ဖျက်သိမ်းရန်</s:a></td>
		</tr>
	</table>
	 <table align="center" width="100%">
		
		<s:form action="insert" >
		<tr>
			<td></td>
			<td><s:textfield name="date" key="label.startDate" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><s:textfield name="name" key="label.name" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><s:textfield name="nrc" key="label.nrc" /></td>
			<td></td>
		</tr>
		<tr>
			<td><s:textfield name="dob" key="label.dob" /></td>
		</tr>
		<tr>
			<td></td>
			<td><s:textfield name="phone" key="label.phone" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><s:textfield name="email" key="label.email" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><s:radio name="gen" key="label.gen" list="{'Male','Female'}" /></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td><s:textarea name="add" key="label.add" cols="25" rows="2" /></td>
			<td></td>
		</tr>
		<tr>
			<td><s:submit name="submit" key="label.insert"   align="center"/></td>
		</tr>
	</s:form>
	</table>
	 
</body>
</html>