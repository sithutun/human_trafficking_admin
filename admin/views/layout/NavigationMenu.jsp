<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<div id="menulist">
<ul>
	<li>
			<s:url action="checkCaseAction" var="cc" />
			<s:a href="%{cc}">တိုင်ကြားစာစီစစ်ရန်</s:a>
	</li>
	<li>
			<s:url action="viewReportAction"  var="viewReport" />
			<s:a href="%{viewReport}">အဖွဲ့၀င်အစီရင်ခံစာကြည့်ရန်</s:a>		
	</li>
	<li>
			<s:url action="publishAction" var="publish" />
			<s:a href="%{publish}">သတင်းထုတ်ပြန်ရန် </s:a>
	</li>
	<li>
			<s:url action="manageMemberAction" var="mm" />
			<s:a href="%{mm}"> အဖွဲ့ဝင်များ</s:a>		
	</li>
	

</ul>
	
</div>
