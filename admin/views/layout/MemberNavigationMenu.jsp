<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<div id="menu">
<ul><li>
			<s:url action="homeAction" var="home" />
			<s:a href="%{home}">ပင်မစာမျက်နှာ</s:a>			
	</li>
	<li>
			<s:url action="memberinformAction" var="inform" />
			<s:a href="%{inform}">တိုင်ကြားရန် </s:a>		
	</li>
	
	<li><s:url action="message_mainAction" var="message" /> <s:a
				href="%{message}">သတင်းအချက်အလက်</s:a>
	</li>
		
	
	<li>
			<s:url action="lawAction" var="law" />
			<s:a href="%{law}">ဥပဒေ</s:a>	
	</li>
	
	<li>
			<s:url action="contactAction" var="contactUs" />
			<s:a href="%{contactUs}">ဆက်သွယ်ရန် </s:a>
	</li>
	<li>
			<s:url action="aboutAction" var="about" />
			<s:a href="%{about}"> အဖွဲ့အစည်းအကြောင်း</s:a>
	</li>
	
		<li>
			<s:url action="logoutAction" var="login" />
			<s:a href="%{login}"> စနစ်မှ ထွက်ရန်</s:a>
	</li>

</ul>
	
</div>
