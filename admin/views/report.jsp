 <%@ page contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Prepare Report</title>
</head>
<body  bgcolor="green">
<h1 align="center">အစီရင်ခံစာ</h1>
<s:form action="reportAction">

<table align="center" >
	<tr>
		<td>အစီရင်ခံစာ အမှတ်</td>
	    <td><s:textfield name="reportid"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>အဖွဲ့၀င်အမည်</td>
	    <td><s:textfield name="membername"  cols="50" rowa="6"/></td>
	    <td></td>
	</tr>
	<tr>
		<td>အကြောင်းအရာ</td>
		<td><s:textarea cols="50" rows="10"  name="reportarea" /></td>
		<td></td>
	</tr>
	<tr>
	<td></td>
	<td><s:submit value=" အစီရင်ခံစာ တင်သွင်းရန်"  align="center"/></td> 
	</tr>
	<tr>
	<td></td>
	<td><s:submit value="မလိုအပ်ပါက ပယ်ဖျက်ရန်"  align="center"/></td> 
	</tr>
	
</table>
<%-- <div align="center">
		<s:submit key="label.submit" align="center"/>
</div> --%>
</s:form>
</body>
</html>