<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><style type="text/css">
.detail{display: none; 
}

a:HOVER .detail {
display: block;	
list-style-type: none;
text-decoration: none;
}
td{
vertical-align: top;}

</style>
</head>
<body>
<div id="cases">
<s:url action="caseDetailAction"  var="caseDetail"></s:url>
<table border="1px">
<tr><td>ရက်စွဲ</td><th>အမှုအမျိုးအစား</th><th>လက်ခံနေရာ</th><th>မှုခင်းနေရာ</th></tr>
<tr><td>၂၀-၉-၂၀၁၄</td><td>အလုပ်သမားခေါင်းပုံဖြတ်မှု</td><td>တရုတ်</td><td>ရန်ကုန်</td><td><s:url action="caseDetailAction"  var="caseDetail"></s:url><s:a href="%{caseDetail}">အသေးစိတ်အချက်အလက်</s:a></td></tr>
<tr><td>၂၃-၉-၂၀၁၄</td><td>ပြည့်တန်ဆာပြုခြင်း</td><td>ထိုင်း</td><td>မန္တလေး</td><td><s:a href="%{caseDetail}">အသေးစိတ်အချက်အလက်</s:a></td></tr>
<tr><td>၂၉-၉-၂၀၁၄</td><td>ကြွေးမြီဖြင့်နှောင်ဖွဲ့ခိုင်းစေခြင်း</td><td>မလေးရှား</td><td>မြစ်ကြီးနား</td><td><s:a href="%{caseDetail}">အသေးစိတ်အချက်အလက်</s:a></td></tr>
</table>
</div>

</body>
</html>