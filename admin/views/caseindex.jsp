 <%@ page contentType="text/html; charset=UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Case</title>
</head>
<body  bgcolor="pink">
<img src="img/img1.jpg">
<h3>လူကုန်ကူးမူ တားဆီးရေး ကေ့စ်ဖိုင်</h3>
<s:url action="caseAction"  var="casefile"></s:url>
<s:a href="%{casefile}">လူကုန်ကူးမူ အမူတွဲ</s:a>
<br><br>
<s:url action="searchcaseAction" var="searchcase"></s:url>
<s:a href="%{searchcase}">ကေ့စ်ဖိုင် ရှာဖွေရန်</s:a>
<br><br>

	<s:url action="updatecaseAction" var="updatecase"></s:url>
	<s:a href="%{updatecase}">အမူတွဲဖိုင် ပြုပြင်ရန်</s:a>
	<br><br>
	
	<s:url action="publicAction" var="publicinformation"></s:url>
	<s:a href="%{publicinformation}">အသိပညာ ပေးရန်</s:a>
	<br><br>
	<s:url action="reportAction" var="report"></s:url>
	<s:a href="%{report}">အစီရင်ခံစာ</s:a>
	<br><br>
	
	<s:url action="postreportAction" var="postreport"></s:url>
	<s:a href="%{postreport}">အစီရင်ခံစာ တင်ပြခြင်း</s:a>
	<br><br>
</body>
</html>