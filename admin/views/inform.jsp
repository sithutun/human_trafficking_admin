
<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<h1>Report About Human Trafficking</h1>
<div id="tip"
	style="FONT-SIZE: medium; FONT-FAMILY: 'Arial Unicode MS'; width: 497px;">
	<p>If you believe you are a victim of human trafficking or may have
		information about a potential trafficking situation, please contact
		us.</p>
</div>
<br>
<h3>Call the Hotline</h3>
<p>
	You can contact us by calling our hotline at <b>01-123456, 02-54321</b>.
	Our hotline agents are available <b>24 hours a day, 7 days a week,
		365 days a year </b>to take reports and to provide the full picture and
	makes sure the best use can be made of your information any where in
	the country related to potential human trafficking victims, suspicious
	behivors, and/or locations where trafficking is suspected to occur. All
	reports are confidential. You can also fill out online form below.
</p>
<h3>Online Reporting Form</h3>
<hr>

<s:form>
	<p>Details Trafficking Report</p>
	<table width="80%">
		<tr>
			<td>Type of Trafficking</td>
			<td><s:select name="trafficking_type"
					list="{'Sex','Labour','Sex and Labour','Other'}" /></td>
		</tr>
		<tr>
			<td>Number of Potential Victims</td>
			<td><s:textfield name="num_of_victims" /></td>
		</tr>

		<tr>
			<td>Location of Trafficking - City</td>
			<td><s:textfield name="trafficking_city" /></td>
		</tr>

		<tr>
			<td>Location of Traffickers - State</td>
			<td><s:select name="case_location"
					list="{'Kachin','Kayah','Kayin','Chin','Sagaing','Thaninthayi','Bago','Magway','Mandalay','Mon','Rakine','Yangon','Shan','Ayeyarwady'}" /></td>
		</tr>
		<tr>
			<td>Number of Traffickers</td>
			<td><s:textfield name="num_of_traffickers" /></td>
		</tr>
		<tr>
			<td>Destination Country</td>
			<td><s:textfield name="destination_country" /></td>
		</tr>

		<tr>
			<td>Opinion of case</td>
			<td><s:textarea rows="2" cols="20" /></td>
		</tr>
		<tr>
			<td>When it happen</td>
			<td><s:textfield name="happened_date" /></td>
		</tr>
		<tr>

			<p>Check the following information all that you see in Situation</p>

		</tr>
		<tr>
			<td><s:checkbox name="involves_female" label="Involves Females" /></td>
			<td><s:checkbox name="involves_male" label="Involves Males" /></td>
		</tr>
		<tr>
			<td><s:checkbox name="involves_adult" label="Involves Adults" /></td>
			<td><s:checkbox name="involves_child" label="Involves Children" /></td>
		<tr>
		<tr>
			<p>
				<label>Description:</label>
			</p>
		<tr>
		<tr>
			<td><s:textarea rows="7" cols="30" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><s:submit name="submit"
					value="Submit" /></td>
		</tr>
	</table>
	<hr>
	<h3>Reporter Information</h3>
	<p>Please provide your information. We may contact you to ask
		additional questions.</p>
	<table>
		<tr>
			<td>Your Name</td>
			<td><s:textfield name="reporter_name"></s:textfield></td>
		</tr>
		<tr>
			<td>Your NRC No.</td>
			<td><s:textfield name="reporter_nrc"></s:textfield></td>
		</tr>
		<tr>
			<td>Your Email</td>
			<td><s:textfield name="reporter_email"></s:textfield></td>
		</tr>
		<tr>
			<td>Your Ph No.</td>
			<td><s:textfield name="reporter_phno"></s:textfield></td>
		</tr>
		<tr>
			<td>Your Address</td>No.
			<td><s:textfield name="reporter_address_no"></s:textfield>,&nbsp;
				<s:textfield name="reporter_address_street"></s:textfield>&nbsp;St.,&nbsp;
				<s:textfield name="reporter_address_quater"></s:textfield>&nbsp;,&nbsp;
			</td>
		</tr>
	</table>

</s:form>