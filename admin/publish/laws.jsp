<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.detail{display: none; 
}

a:HOVER .detail {
display: block;	
list-style-type: none;
text-decoration: none;
}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
table{
margin-top:10px;
width:100%;
padding: 6px;

}

</style>
</head>
<body>
<s:url action="updatelaw" var="update"></s:url>
<s:url action="deletelaw" var="delete"></s:url>
<s:url action="insertlaw" var="insert"></s:url>
<table>
<caption>ဥပဒေများ</caption>
<tr><td>အခန်း(၁)</td><td>အမည်၊စီရင်ပိုင်ခွင့်နှင့်အဓိပ္ပါယ်​​ဖော်ပြချက်</td><td><s:a href="%{delete}">ဖျက်မည်</s:a></td><td><s:a href="update">ပြင်ဆင်မည်</s:a></td></tr>
<tr><td>အခန်း(၂)</td><td>ရည်ရွယ်ချက်များ</td><td><s:a href="delete">ဖျက်မည်</s:a></td><td><s:a href="%{update}">ပြင်ဆင်မည်</s:a></td></tr>
<tr><td>အခန်း(၃)</td><td>ဗဟိုအဖွဲ့ ဖွဲ့စည်းခြင်းနှင့် လုပ်ငန်းတာဝန်များ</td><td><s:a href="delete">ဖျက်မည်</s:a></td><td><s:a href="update">ပြင်ဆင်မည်</s:a></td></tr>
<tr><td><s:a href="%{insert}"> အသစ်ထည့်မည် </s:a></td><td></td><td></td></tr>
</table>

</body>
</html>