<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>

</head>
<body><s:url action="insertlaw" var="insert"></s:url>
<table>
<caption>ဥပဒေများ</caption>

<tr><td>အခန်း(၂)</td><td>ရည်ရွယ်ချက်များ</td><td><s:a href="delete">ဖျက်မည်</s:a></td><td><s:a href="update">ပြင်ဆင်မည်</s:a></td></tr>
<tr><td>အခန်း(၃)</td><td>ဗဟိုအဖွဲ့ ဖွဲ့စည်းခြင်းနှင့် လုပ်ငန်းတာဝန်များ</td><td><s:a href="delete">ဖျက်မည်</s:a></td><td><s:a href="update">ပြင်ဆင်မည်</s:a></td></tr>
<tr><td><s:a href="%{insert}">အသစ်ထည့်မည်</s:a></td><td></td><td></td></tr>
</table>
<s:url action="homeAction" var="home"></s:url>
<s:a href="%{home}"></s:a>
<s:url action="back" var="back"></s:url>
<s:a href="%{back}"></s:a>
</body>
</html>