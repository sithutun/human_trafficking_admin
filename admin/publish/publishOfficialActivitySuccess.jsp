<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.detail{display: none; 
}

a:HOVER .detail {
display: block;	
list-style-type: none;
text-decoration: none;
}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
table{
margin-top:10px;
width:100%;
padding: 6px;

}

</style>
</head>
<body>
<s:form theme="simple">
<table><caption>ထုတ်ပြန်မှုအောင်မြင်</caption>
<tr><td>ရက်စွဲ</td><td><s:property value="p_info.public_date"/> </td></tr>
<tr><td>ခေါင်းစဉ်</td><td><s:property value="p_info.title"/></td></tr>
<tr> <td>​​ဖော်ပြချက် </td>
<td><s:property value="p_info.description"/> </td>
</tr>
<tr> <td>အသေးစိတ်​ဖော်ပြချက် </td>
<td><s:property value="p_info.detail_description" /> 
</td>
 </tr>

<tr> <td><s:url action="publishAction" var="back"></s:url><s:a href="%{back}">ရှေ့သို့</s:a>  </td><td></td></tr>
</table>
</s:form>

</body>
</html>