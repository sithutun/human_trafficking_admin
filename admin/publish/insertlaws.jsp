<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
    <html>
<head>
<style type="text/css">
.detail{display: none; 
}

a:HOVER .detail {
display: block;	
list-style-type: none;
text-decoration: none;
}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
table{
margin-top:10px;
width:100%;
padding: 6px;

}

</style>
</head>
<body>
<s:form action="updateSuccess" theme="simple">
<table >
<caption>ဥပဒေအသစ်ထည့်ရန်</caption>
<tr>
<td>အခန်းအမှတ်စဉ်</td>
<td><s:textfield name="chapter" ></s:textfield></td>
<td></td>
</tr>
<tr>
<td>ခေါင်းစဉ်</td>
<td><s:textfield name="title"></s:textfield></td>
<td></td>
</tr>
<tr>
<td>အကြောင်းအရာ</td>
<td><s:textarea name="description"  rows="30" cols="50"></s:textarea></td>
<td></td>
</tr>
<tr colspan="3">
<td><s:submit value="ထည့်သွင်းခြင်း"></s:submit></td>
</tr>
</table>
</s:form>

</body>
</html>