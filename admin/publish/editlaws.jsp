<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ဥပဒေ ပြင်ဆင်ခြင်း</title>
</head>
<body>
<s:form action="updateSuccess"  theme="simple">
<s:textfield name="chapter" value="အခန်း(၁)" label="အခန်းအမှတ်စဉ်"/><br>
<s:textfield name="title" value="အမည်၊ စီရင်ပိုင်ခွင့်နှင့် အဓိပ္ပာယ် ဖော်ပြချက်" label="ခေါင်းစဉ်"/><br>
<s:textarea name="description" value="ဤဥပဒေကို လူကုန်ကူးမှု တားဆီးကာကွယ်ရေး ဥပဒေဟု...." label="အကြောင်းအရာ" rows="50" cols="50"/><br>
<s:submit value=" ပြင်ဆင်သိမ်းမည်"></s:submit>
</s:form>
</body>
</html>