<%@ page contentType="text/html;charset=UTF-8" language="java"
pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<style type="text/css">
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
input[type="submit"]{
padding: 10px;

}
th{
padding: 6px;}
td{
vertical-align: top;
font-size: 14px;
padding: 6px;
}
nav a{
text-decoration: none;
line-height: 55px;
padding: 10px;
width:100px;
font-size: 20px;
box-shadow:2px 3px 4px black;
}
nav a:hover{
text-decoration: none;}
</style>
</head>
<body bgcolor="lightgreen">
<s:form action="updateMemberSuccess"   enctype="multipart/form-data"  theme="simple">
<s:hidden name="member.division.division_name" value="%{member.division.division_name}" />
<s:hidden name="member.member_id" value="%{id}"></s:hidden>
	<table >
	<caption>အဖွဲ့ဝင်အချက်အလက်အသေးစိတ်ပြင်ဆင်ရန်</caption>
	<tr><td><img alt="ဓာတ်ပုံ" src="<s:property value="member.personal_Detail.photo" />"  width="150px"  height="200px"></td><td></td></tr>
<tr>
			<td>ပုံပြောင်းရန်</td>
			<td><s:file name="upload.image" ></s:file> </td>
			<td></td>
		</tr>
	<tr>
			<td>အဖွဲ့ဝင်နာမည်</td>
			<td><s:textfield name="member.member_name"  /></td>
			<td></td>
		</tr>
		
		<tr>
			<td>နာမည်အရင်း</td>
			<td><s:textfield readonly="true" name="member.name"   /></td>
			<td></td>
		</tr>
<tr>
			<td>အဖအမည်</td>
			<td><s:textfield readonly="true" name="member.personal_Detail.father_name"  /></td>
			<td></td>
		</tr>
<tr>
			<td>ရာထူး</td>
			<td><s:textfield name="member.position"  /></td>
			<td></td>
</tr>
		<tr>
			<td>မှတ်ပုံတင်</td>
			<td><s:textfield readonly="true" name="member.personal_Detail.nrc" /></td>
			<td></td>
		</tr>
		<tr><td>မွေးသက္ကရာဇ်</td>
			<td><s:textfield readonly="true" name="member.personal_Detail.dob"  /></td>
		</tr>
		<tr>
			<td>ဖုန်း</td>
			<td><s:textfield name="member.personal_Detail.address.phone" /></td>
			<td></td>
		</tr>		
	<tr>
			<td colspan="2">လိပ်စာ</td><td></td></tr>
		<tr><td>
			<label>အိမ်အမှတ်</label></td><td><s:textfield name="member.personal_Detail.address.home_no"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>လမ်း</label></td><td><s:textfield name="member.personal_Detail.address.street"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>ရပ်ကွက်</label></td><td><s:textfield name="member.personal_Detail.address.quarter"></s:textfield>
			</td><td></td></tr>
		<tr><td>
			<label>မြို့</label></td><td><s:textfield name="member.personal_Detail.address.town"></s:textfield>
			</td><td></td></tr>
		<tr><td>
		<s:url action="memberDetailAction" method="memberDetail"  var="md">
		<s:param name="id" value="member.member_id"/>
		
		</s:url> 
		<nav><s:a href="%{md}">နောက်သို့</s:a></nav></td>
			<td><s:submit value="ပြင်ဆင်မည်"  align="center" /></td>
			
		</tr>
	</table>
	</s:form>
</body>

</html>