<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#but{
font-size: 20px;
line-height: 90px;
width: 200px;
margin:5px;
padding:10px;
background-color: #eafo22;
border-radius:6px;
box-shadow:2px 3px 6px black;
text-decoration: none;
}
#but:hover{
background-color: #ea40ff;

}
input[type="text"]{
font-size:12px;
padding: 10px;
}
td{
font-size: 14px;
spacing:5px;
padding: 8px;
}
caption{
border-radius:6px;
box-shadow:2px 3px 5px black;
background:#aaccee;
line-height:60px;
width:100%;
margin-bottom: 5px;
font-size: 30px;
font-weight: bold;
margin-bottom: 10px;
}
h1{
padding: 10px;
}
</style>
</head>
<body><s:form theme="simple"> 
<h1>
ပေးပို့မှုအောင်မြင်</h1>
<table><tr><td>ရက်စွဲ</td><td>
<s:textfield name="instruction.c.inform_date" readonly="true"   /></td></tr><tr><td>လူကုန်ကူးမှုအမျိုးအစား</td><td>
<s:textfield  readonly="true"  name="instruction.c.trafficking_type"  ></s:textfield></td></tr><tr><td>လူကုန်ကူးမည့်နိုင်ငံ</td><td>
			
<s:textfield readonly="true"  name="instruction.c.destination_country" ></s:textfield></td></tr><tr><td>	အမှုဖြစ်ပွားရာမြို့</td><td>
<s:textfield readonly="true" name="instruction.c.case_location" ></s:textfield></td></tr><tr><td>အမှုဖြစ်ပွားရာပြည်နယ်/တိုင်း</td><td>
<s:textfield readonly="true" name="instruction.c.division.division_name" ></s:textfield></td></tr><tr><td>ပါဝင်သည့်လူများ</td><td>
<s:textfield readonly="true" name="instruction.c.person_in_case" ></s:textfield></td></tr><tr><td>ထင်မြင်ယူဆချက်</td><td>
<s:textarea readonly="true"  name="instruction.c.description"  cols="30" ></s:textarea ></td></tr><tr><td>အသေးစိတ်ထပ်မံ​​ဖော်ပြချက်</td><td>
<s:textarea readonly="true" name="instruction.c.detail_description"   cols="30"></s:textarea></td></tr><tr><td>ဖော်ပြချက်(မှတ်ချက်) </td><td>
<s:textarea readonly="true" name="instruction.remark"  cols="30"></s:textarea> </td></tr></table>
</s:form>
<s:url action="homeAction"  var="checkinform"></s:url>
<s:a href="%{checkinform}" id="but">တိုင်ကြားစာစိစစ်ရန်သို့</s:a>
</body>
</html>