package dbservice;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import dbservice.HibernateUtil;

import entities.Admin;
import entities.Case;
import entities.Division;
import entities.Instruction;
import entities.Member;
import entities.Public_Information;
import entities.Report;
import entities.Trafficker_victim;

public class DBOperation {
	Session session=null;
	Transaction tx=null;
	private List<Case> caselist=new ArrayList<Case>();
	private Report report;
	
	@SuppressWarnings("unchecked")
	public List<Case> retrieveCase()
	{
		try {
			session=HibernateUtil.getSession();
			System.out.println("opened session");
			
			tx=session.beginTransaction();
			
			Query q=session.createQuery("from Case c where c.status is NULL");
			
			caselist=(ArrayList<Case>)q.list();
			tx.commit();
			System.out.println(caselist.size());
			return caselist;
		} catch (Exception e) {
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
			return null;
		}
		finally
		{
			session.close();
		}
	}
	//insert Public Information
			public boolean insertInformation(Public_Information pi)
				{
					try
					{
						session=HibernateUtil.getSession();
						System.out.println("session is opened.");
						tx=session.beginTransaction();
						session.persist(pi);
						tx.commit();
						System.out.println("Save Successfully");
						return true;
					} catch (Exception e) 
					{
						if(tx!=null)
							tx.rollback();
						e.printStackTrace();
						return false;
					}
					finally
					{
						session.close();
					}
				}
	public boolean insertNewMember(Member member){
		
		try{
			session=HibernateUtil.getSession();
			System.out.println("session is opened.......");
			tx=session.beginTransaction();
			session.persist(member.getPersonal_Detail());
			tx.commit();
			tx=session.beginTransaction();
			session.persist(member);
			tx.commit();
			System.out.println("Save Successfully");
			return true;
		}catch(Exception e){
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
			return false;
		}finally{
			session.close();
		}
	}
	//retrive division From Report Action retrieveDivisionFromReport
	@SuppressWarnings("unchecked")
	public List<Division>retrieveDivisionFromReport()
	{List<Division> divlist=new ArrayList<>();
try{
			
			session=HibernateUtil.getSession();
			System.out.println("Session is opened!");
			/*SQLQuery sql=session.createSQLQuery("select distinct r.c.division.division_name from Report r");
			sql.addEntity(Report.class);*/
			Query query=session.createQuery("select distinct r.c.division.division_name from Report r");
			
			tx=session.beginTransaction();
			List<String> divString=query.list();
			
			for(String div: divString){	
				divlist.add(new Division(div));
			}
			tx.commit();
			System.out.println(divlist.size());
			System.out.println("Success");
			return divlist;
		}
		catch(Exception ex)
		{
			if(tx!=null)
			{
				tx.rollback();
			}
			ex.printStackTrace();
			return null;
		}
		finally
		{
			session.close();
		}
	}
	public Report updateReportStatus(int id){
		Report rep = null;
		try{
			session=HibernateUtil.getSession();
			rep=(Report) session.get(Report.class, id);
			rep.setStatus(1);
			tx=session.beginTransaction();
			session.update(rep);
			tx.commit();
			
		}catch(Exception e){
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}finally{
			session.close();
		}return rep;
	}
	//retrieve division
		@SuppressWarnings("unchecked")
		public List<Division> retrieveDivision()
		{List<Division> divlist;
	try{
				
				session=HibernateUtil.getSession();
				System.out.println("Session is opened!");
				Query query=session.createQuery("from Division ");
				tx=session.beginTransaction();
				divlist=query.list();
				tx.commit();
				System.out.println("Success");
				return divlist;
			}
			catch(Exception ex)
			{
				if(tx!=null)
				{
					tx.rollback();
				}
				ex.printStackTrace();
				return null;
			}
			finally
			{
				session.close();
			}
		}
		
/*																		   Select Case Detail 															 */
public Case getCaseDetail(int id){
			Case c=null;
			try{
				session=HibernateUtil.getSession();
				System.out.println("Got Session");
				tx=session.beginTransaction();
				c=(Case)session.get(entities.Case.class, id);
				tx.commit();
				return c;
			}catch(Exception e){
				if(tx!=null){
					tx.rollback();
					e.printStackTrace();
					return c;
				}
			}finally{
				session.close();
			}
			return c;
			
		}
/*                                                               //View Report                                           //public List<>                 */	 
@SuppressWarnings("unchecked")
		public List<Member> retrieveAllMembers(){
			List<Member> memList=null;
			try{
				session=HibernateUtil.getSession();
				Query q=session.createQuery("from Member");
				tx=session.beginTransaction();
				memList=q.list();
				tx.commit();
				return memList;
			}catch(Exception e){
				if(tx!=null)
				tx.rollback();
				e.printStackTrace();
				return memList;
			}finally{
				session.close();
			}
		}
//search member by Division
		@SuppressWarnings("unchecked")
		public List<Member> retrieveMembersByDiv(String idString){
			List<Member> memList=null;
			try{
				session=HibernateUtil.getSession();
				System.out.println("Session Object Got ...");
			Query q=session.createQuery("select m from Member m,Division d where m.division.division_name like d.division_name and  d.division_name like :id");
				q.setParameter("id", idString);
				tx=session.beginTransaction();
				memList=q.list();
				tx.commit();
				return memList;
			}catch(Exception e){
				if(tx!=null)
					tx.rollback();
				e.printStackTrace();
				return null;
			}finally{
				session.close();
			}
			
		}
		
/*																							//Sending Instruction   */
public Instruction sendInstruction(Instruction instruction){
			
			System.out.println("SendInstruction");
			try{
				
				session=HibernateUtil.getSession();
				Query q=session.createQuery("update Case c set  c.status=? where c.case_id=?");
				q.setParameter(0, "စုံစမ်းဆဲ");
				q.setParameter(1, instruction.getC().getCase_id());
				System.out.println("Got Sessiono object......");
				tx=session.beginTransaction();
				q.executeUpdate();
				session.persist(instruction);				
				session.flush();
				tx.commit();
				System.out.println("Instruction send Success");			
			
		instruction.setC( (Case)session.get(Case.class, instruction.getC().getCase_id()));
		System.out.println(instruction.getInstruction_id()+"  and  "+instruction.getC().getCase_location());
			return instruction;
			}catch(Exception e){
				if(tx!=null)
					tx.rollback();
				e.printStackTrace();
				return null;
			}finally{
				session.close();
			}
		}
		//Updating Member
		public  boolean updateMember(Member member){
			try{				
				session=HibernateUtil.getSession();
				tx=session.beginTransaction();
				session.update(member.getPersonal_Detail());
				session.update(member);
				tx.commit();
				return true;
			}catch(Exception e){
				if(tx!=null)
					tx.rollback();
				e.printStackTrace();
				return false;
				
			}finally{
				session.close();
			}
			
		}
///Select Admin
		public Admin selectAdmin(Admin admin){
			Admin a=null;
			try{			
				session=HibernateUtil.getSession();
				/*Query q=session.createQuery("from Admin");	
				a= (Admin) q.list().get(0);*/
				a=(Admin)session.get(Admin.class, admin.getName());				
				System.out.println(a.getName()+a.getPassword());
			return a;
			}catch(Exception e){
				if(tx!=null)
				tx.rollback();
				e.printStackTrace();
				return a;
			}finally{
				session.close();
			}
			
			
		}
		//Deleting Member by id
		public boolean deleteMember(int id){
			try{
				session=HibernateUtil.getSession();
				
				tx=session.beginTransaction();
				session.delete(session.get(Member.class, id));
				tx.commit();
				return true;
			}catch(Exception e){
				if(tx!=null)
					tx.rollback();
				e.printStackTrace();
				return false;
			}finally{
				session.close();
			}
		}
		//Query for specific member
 		public Member retrieveMemberDetail(int id){
 			Member member;
			try{
				session=HibernateUtil.getSession();
				System.out.println("Got Session Object..............."+id);
				member= (Member)session.get(Member.class,id);
				System.out.println(member.getMember_name());
				return member;
			}catch(Exception e)
			{
				if(tx!=null)
					tx.rollback();
				System.out.println("retrieve Member Detail Fail");
				e.printStackTrace();
				return null;
			}finally{
				session.close();
				System.out.println("Retrieve Member Deatil");
			}
		}
	@SuppressWarnings("unchecked")
	public List<Member> retrieveMembers(String idString){
		List<Member> memList=null;
		try{
			session=HibernateUtil.getSession();
			System.out.println("Got Session Obj");
			Query q= session.createQuery("from Member where division.division_name like ?");
			q.setParameter(0, idString);
			tx=session.beginTransaction();
			memList=q.list();
			tx.commit();
			return memList;
			
		}catch(Exception e){
			if(tx!=null){
				tx.rollback();
				e.printStackTrace();
				return memList;
			}
		}finally{
			session.close();
		}
		return memList;
		
	}
	
	// +++++++++CONCERN WITH REPORT+++++++++//

	@SuppressWarnings("unchecked")
	public List<Report> retrieveallreport() {
		List<Report> rlist = null;
		try {
			session = HibernateUtil.getSession();
			System.out.println("Report  Session is opened!");
			Query query = session.createQuery("from Report ");
			System.out.println("After Report Query!!!!!!!!!!");
			rlist = (List<Report>) query.list();
			System.out.println("REpor Lists " + rlist);
			System.out.println(rlist);
			System.out.println("!!+++++++++++ Report Success !!");
			return rlist;
		} catch (Exception ex) {
			System.out.println(ex);
			if (tx != null) {
				tx.rollback();
				return rlist;
			}
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rlist;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Report> retrievereport(String divname) {
			List<Report> rlist = null;
			try {
				session = HibernateUtil.getSession();
				System.out.println("Report  Session is opened!");
				Query query = session
						.createQuery("select r from Report r where  r.member.division.division_name like ?");
				query.setParameter(0, divname);
				tx = session.beginTransaction();
				tx.commit();
				System.out.println("After Report Query!!!!!!!!!!");
				rlist = (List<Report>) query.list();
				System.out.println("REpor Lists " + rlist);
				System.out.println(rlist);
				System.out.println("!!+++++++++++ Report Success !!");
				return rlist;
			} catch (Exception ex) {
				System.out.println(ex);
				if (tx != null) {
					tx.rollback();
					return rlist;
				}
				ex.printStackTrace();
			} finally {
				session.close();
			}
			return rlist;
		}

		public Report retrieveOneReport(int reportId) {
			try {
				session = HibernateUtil.getSession();
				System.out.println("Session  Opened");
				System.out.println(reportId);
				report = (Report) session.get(Report.class, reportId);
				System.out.println(report);
				return report;
			} catch (Exception e) {
				if (tx != null)
					tx.rollback();
				e.printStackTrace();
				return report;
			} finally {
				session.close();
			}
		}
		// +++++++++++++++++Concern With T & V++++++++++++++++//

		public boolean saveTV(Trafficker_victim tv) {

			try {
				session = HibernateUtil.getSession();
				System.out.println("Session  Opened On Saveing TVVV");
				tx = session.beginTransaction();
				session.persist(tv);
				tx.commit();
				System.out.println("Saveing Report Good...");
				return true;
			} catch (Exception e) {
				if (tx != null)
					tx.rollback();
				e.printStackTrace();
				return false;
			} finally {
				session.close();
			}
		}
/*                                                         Adding Respondent and Savior                                     */		
public boolean addRespondentVictim(Trafficker_victim tv){
	try{
		System.out.println("Inside addRespondent Victim DBSERVICe");
		session=HibernateUtil.getSession();
		System.out.println("Got Session Object");
		tx=session.beginTransaction();
		session.persist(tv.getPdetail());
		tx.commit();
		System.out.println("Success pdetail");
		tx=session.beginTransaction();
		session.persist(tv);
		tx.commit();
		System.out.println("Save TV Success");
		return true;
	}catch(Exception e)
	{
		if(tx!=null)
			tx.rollback();
		e.printStackTrace();
		return false;
	}finally{session.close();}
}
/*  Retrieve All Traffickers and Victims*/
public List<Trafficker_victim> retrieveAllPeople(int id){
	try{
		session=HibernateUtil.getSession();
		Query q=session.createQuery("from Trafficker_victim tv where rv_id="+id);
		return (List<Trafficker_victim>)q.list();
	}catch(Exception e){
		if(tx!=null)
			tx.rollback();
		return null;
	}finally{
		session.close();
	}	
}

//retrive division From Report Action retrieveDivisionFromReport
	@SuppressWarnings("unchecked")
	public List<Division> retrieveDivisionFromMember()
	{List<Division> divlist=new ArrayList<>();
try{
			
			session=HibernateUtil.getSession();
			System.out.println("Session is opened!");
			Query query=session.createQuery("select distinct m.division.division_name from Member m");
			
			tx=session.beginTransaction();
			List<String> divString=query.list();
			
			for(String div: divString){	
				divlist.add(new Division(div));
			}
			tx.commit();
			System.out.println(divlist.size());
			System.out.println("Success");
			return divlist;
		}
		catch(Exception ex)
		{
			if(tx!=null)
			{
				tx.rollback();
			}
			ex.printStackTrace();
			return null;
		}
		finally
		{
			session.close();
		}
	}
}