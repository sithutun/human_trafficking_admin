package dbservice;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Member;
import entities.Report;

public class DAO_Report {
	
	Session session = null;
	Transaction tx = null;
	
	// to save Report
	public void saveReport(Report report ) {
		try {
			
			HttpSession ss = ServletActionContext.getRequest().getSession(true);
			if (ss.getAttribute("loginedMember") != null) 
				report.setMember((Member) ss.getAttribute("loginedMember"));
				
			session = HibernateUtil.getSession();
			System.out.println("Report session is opened.");
			tx = session.beginTransaction();
			session.persist(report);
			tx.commit();
			System.out.println("Report returning True...");
			} 
			catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		
		} finally {
			session.close();
		}
	
}
}
