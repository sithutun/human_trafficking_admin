package entities;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class Address {
	
	private String home_no;
	private String street;
	private String quarter;
	private String town;
	private String phone;
	private String div;
	@SuppressWarnings("unused")
	private String maddress;
		public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getHome_no() {
		return home_no;
	}
	public void setHome_no(String home_no) {
		this.home_no = home_no;
	}
	public String getQuarter() {
		return quarter;
	}
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getDiv() {
		return div;
	}
	public void setDiv(String division) {
		this.div = division;
	}
	@Transient
	public void setMaddress(String maddress) {
		this.maddress = maddress;
	}
	public String getMaddress(){
		return "အိမ်အမှတ်("+home_no+")\n"+street+quarter+"\n"+town+div;
	}
	
	public Address() { }
	
	
}
