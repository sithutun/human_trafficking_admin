package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Trafficker_victim implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int rv_id;
	private String type;
	private String nickname;
	private String rv_name;
	private Case c;
	private Personal_Detail pdetail;

	public Trafficker_victim() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getRv_id() {
		return rv_id;
	}

	public void setRv_id(int rv_id) {
		this.rv_id = rv_id;
	}

	@ManyToOne
	public Case getC() {
		return c;
	}

	public void setC(Case c) {
		this.c = c;
	}

	@OneToOne
	public Personal_Detail getPdetail() {
		return pdetail;
	}

	public void setPdetail(Personal_Detail pdetail) {
		this.pdetail = pdetail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getRv_name() {
		return rv_name;
	}

	public void setRv_name(String rv_name) {
		this.rv_name = rv_name;
	}

}
