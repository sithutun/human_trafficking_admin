package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;

@Entity
public class Informer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int informer_id;
	private String name;
	private String nrc;
	private String gender;
	private String email;
	private String phone_no;
	private String address;

	
	@Id
	@SequenceGenerator(name="seqInformerid", sequenceName="seqid", allocationSize=1)
	@GeneratedValue (strategy=GenerationType.SEQUENCE,generator="seqInformerid")
	public int getInformer_id() {
		return informer_id;
	}
	public void setInformer_id(int informer_id) {
		this.informer_id = informer_id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getNrc() {
		return nrc;
	}
	public void setNrc(String nrc) {
		this.nrc = nrc;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
