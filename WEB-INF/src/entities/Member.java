package entities;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
//@Table(name="HMember")
public class Member implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int member_id;
	private String member_name;
	private String name;
	private String password;
	private String retype_password;
	private String position;
	private Division division;
	private Personal_Detail personal_Detail;

	@Id
	@SequenceGenerator(name="seqMemberId", sequenceName="seqMember", allocationSize=1)
	@GeneratedValue (strategy=GenerationType.SEQUENCE,generator="seqMemberId")
	public int getMember_id() {
		return member_id;
	}
	public String getMember_name() {
		return member_name;
	}

	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}

	@Transient
public String getRetype_password() {
		return retype_password;
	}

	public void setRetype_password(String retype_password) {
		this.retype_password = retype_password;
	}

public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@ManyToOne
public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

@OneToOne
public Personal_Detail getPersonal_Detail() {
	return personal_Detail;
}

public void setPersonal_Detail(Personal_Detail personal_Detail) {
	this.personal_Detail = personal_Detail;
}

public Member() { }	
}
