package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Case implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int case_id;
	private String inform_date;
	private String trafficking_type;
	private String no_trafficker;
	private String no_victim;
	private String case_location;
	private String destination_country;
	private String description;
	private String detail_description;
	private String person_in_case;
	private Division division;
	private Informer info;
	private String status;	
	@Id
	@SequenceGenerator(name="seqCaseid", sequenceName="seqid", allocationSize=1)
	@GeneratedValue (strategy=GenerationType.SEQUENCE,generator="seqCaseid")

	public int getCase_id() {
		return case_id;
	}
	public void setCase_id(int case_id) {
		this.case_id = case_id;
	}
@ManyToOne
	public Informer getInfo() {
		return info;
	}
	public void setInfo(Informer info) {
		this.info = info;	}
	
	public String getInform_date() {
		return inform_date;
	}
	public void setInform_date(String inform_date) {
		this.inform_date = inform_date;
	}
	public String getTrafficking_type() {
		return trafficking_type;
	}
	public void setTrafficking_type(String trafficking_type) {
		this.trafficking_type = trafficking_type;
	}
	
	public String getNo_trafficker() {
		return no_trafficker;
	}
	public void setNo_trafficker(String no_trafficker) {
		this.no_trafficker = no_trafficker;
	}
	public String getNo_victim() {
		return no_victim;
	}
	public void setNo_victim(String no_victim) {
		this.no_victim = no_victim;
	}
	
	public String getCase_location() {
		return case_location;
	}
	public void setCase_location(String case_location) {
		this.case_location = case_location;
	}
	public String getDestination_country() {
		return destination_country;
	}
	public void setDestination_country(String destination_country) {
		this.destination_country = destination_country;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDetail_description() {
		return detail_description;
	}
	public void setDetail_description(String detail_description) {
		this.detail_description = detail_description;
	}
	
	@ManyToOne
	public Division getDivision() {
		return division;
	}
	public void setDivision(Division division) {
		this.division = division;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPerson_in_case() {
		return person_in_case;
	}
	public void setPerson_in_case(String person_in_case) {
		this.person_in_case = person_in_case;
	}
	
	

}
