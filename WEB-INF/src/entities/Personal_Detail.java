package entities;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Personal_Detail {
	private int pdetail_id;
	private String gender;
	private String nrc;
	private String dob;
	private String photo;
	private String father_name;
	private Address address;
	
	@Id
	@SequenceGenerator(name="seqPersonalID",sequenceName="seqPersonal",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seqPersonalID")
	public int getPdetail_id() {
		return pdetail_id;
	}
	public void setPdetail_id(int pdetail_id) {
		this.pdetail_id = pdetail_id;
	}
	public Personal_Detail() {
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNrc() {
		return nrc;
	}
	public void setNrc(String nrc) {
		this.nrc = nrc;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFather_name() {
		return father_name;
	}
	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}
	
	@Embedded
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

}
