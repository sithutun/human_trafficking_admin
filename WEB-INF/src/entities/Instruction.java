package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Instruction {

	private int instruction_id;
	private String send_date;
	private String remark;
	private Case c;
	@Id
	@SequenceGenerator(name="seqInstructionId", sequenceName="seqInstruction", allocationSize=1)
	@GeneratedValue (strategy=GenerationType.SEQUENCE,generator="seqInstructionId")
	public int getInstruction_id() {
		return instruction_id;
	}
	public void setInstruction_id(int instruction_id) {
		this.instruction_id = instruction_id;
	}
	public String getSend_date() {
		return send_date;
	}
	public void setSend_date(String send_date) {
		this.send_date = send_date;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@OneToOne
	public Case getC() {
		return c;
	}
	public void setC(Case c) {
		this.c = c;
	}
	public Instruction() {
	}
	

}
