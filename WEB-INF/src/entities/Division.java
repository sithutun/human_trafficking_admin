package entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Division {
private String division_name;
@Id
	public String getDivision_name() {
		return division_name;
	}
	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}
	public Division() {
	}
	public Division(String div){
		setDivision_name(div);
	}
}
