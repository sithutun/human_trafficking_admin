package actions;

import java.util.ArrayList;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import dbservice.DBOperation;
import entities.Case;
import entities.Division;
import entities.Informer;
import entities.Instruction;
import entities.Member;

public class CaseAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Case c;
	private int id;
	private Instruction instruction;
	private String idString;
	private Informer info;
	private List<Member> memberList = new ArrayList<>();
	private Member member;
	private List<Division> divlist = new ArrayList<Division>();
	private List<Case> caselist = new ArrayList<Case>();
	private DBOperation dao = new DBOperation();

	public Instruction getInstruction() {
		return instruction;
	}

	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}

	public List<Member> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString = idString;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Division> getDivlist() {
		return divlist;
	}
	public void setDivlist(List<Division> divlist) {
		this.divlist = divlist;
	}

	public CaseAction() {
		retrieveCaseFile();
	}

	public Case getC() {
		return c;
	}

	public void setC(Case c) {
		this.c = c;
	}

	public Informer getInfo() {
		return info;
	}

	public void setInfo(Informer info) {
		this.info = info;
	}

	public List<Case> getCaselist() {
		return caselist;
	}

	public void setCaselist(List<Case> caselist) {
		this.caselist = caselist;
	}

	public String retrieveCaseFile() {
		caselist = dao.retrieveCase();
		System.out.println(caselist);
		return SUCCESS;
	}
	/*//View Report 
		public String viewReport(){
			memberList=dao.viewReport();
			System.out.println("View Report");
			return SUCCESS;
			
		}*/
	// select Member by Division
	public String searchMemberByDiv() {
		memberList = dao.retrieveMembersByDiv(idString);
		System.out.println("Inside search member by Division");
		divlist = dao.retrieveDivision();
		return SUCCESS;
	}

	// Seleting All Division
	public String selectDivision() {
		memberList = dao.retrieveAllMembers();
		System.out.println("Inside selectDivision");
		divlist = dao.retrieveDivision();
		return SUCCESS;
	}
	public String selectAllDivision(){
		divlist=dao.retrieveDivision();
		return SUCCESS;
	}
	// To retrieve Case Detail
	public String caseDetail() {
		c = dao.getCaseDetail(id);
		if (c != null)
			return SUCCESS;
		else
			return ERROR;
	}

	// Testing for ID carrying or not
	public String displayID() {
		System.out.println("Case ID is " + id);
		return SUCCESS;
	}

	// Retrieve Member List
	public String retrieveMembers() {
		memberList = dao.retrieveMembers(idString);
		System.out.println("After Retrieve");
		if (memberList != null)
			return SUCCESS;
		else
			return ERROR;
	}

	@Override
	public void validate() {
		System.out.println("valdate mthod");
	}
}
