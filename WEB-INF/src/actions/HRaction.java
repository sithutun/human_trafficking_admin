package actions;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dbservice.DBOperation;

import entities.Admin;
import entities.Case;

public class HRaction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String name,password;
	private Admin admin;
	 private String remark;
	 private DBOperation dao=new DBOperation();
	 private List<Case> caselist=new ArrayList<Case>();
	 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<Case> getCaselist() {
		return caselist;
	}

	public void setCaselist(List<Case> caselist) {
		this.caselist = caselist;
	}
	
	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public String send(){
		System.out.println("Inside HRactionlllllllll");
		System.out.println(remark);
		
		return SUCCESS;
	}
	
	public String execute() throws IOException {
		caselist=dao.retrieveCase();
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		session.setAttribute("adminLogin",admin);		
		return SUCCESS;
	}
	@Override
	public void validate() {
		
			name=admin.getName();password=admin.getPassword();
		admin=dao.selectAdmin(admin);
		if(admin!=null){
	if("".equals(name) || "".equals(password)|| !name.equals(admin.getName()) || !password.equals(admin.getPassword())){
		
			addFieldError("admin.name","နာမည်နှင့်လျို့ဝှက်နံပါတ်မှားနေပါသည်");	
	}}
	else {
		
		addFieldError("admin.name","နာမည်နှင့်လျို့ဝှက်နံပါတ်မှားနေပါသည်");	
	}
	
	}
}