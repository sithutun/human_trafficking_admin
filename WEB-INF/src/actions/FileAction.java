package actions;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
public class FileAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MyFile upload;	
	
	private String path;		
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}	

	public MyFile getUpload() {
		return upload;
	}

	public void setUpload(MyFile upload) {
		this.upload = upload;
	}

	public String execute() throws Exception {
		try{
			//create a new file within specified directory in which uploaded file will be copied
			String p=ServletActionContext.getServletContext().getRealPath("/");
			System.out.println("New File path is"+p);		
		    File file=new File(p +"\\images\\"+upload.getImageFileName());
			//get the file uploaded by user
			File uploadedFile=upload.getImage();
			//use commons IO to Copy the uploadedFile to the new file			
			FileUtils.copyFile(uploadedFile,file);
			path=file.getAbsolutePath();
			System.out.println("Upload File Path is"+path);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			addActionError("Exception occured while creating file");
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	
	
}
