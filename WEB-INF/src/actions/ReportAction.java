package actions;

import java.util.ArrayList;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import dbservice.DBOperation;
import entities.Division;
import entities.Member;
import entities.Report;
import entities.Trafficker_victim;
public class ReportAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Member member;
	private Report report;
	private int reportId;
	private DBOperation dbo = new DBOperation();
	private String divname;
	private List<Report> reportlist = new ArrayList<Report>();
	private List<Division> divlist = new ArrayList<Division>();
	private List<Trafficker_victim> tv_List;
	private Trafficker_victim tv;
	
	
	public List<Trafficker_victim> getTv_List() {tv.getC().getCase_id();
		return tv_List;
	}

	public void setTv_List(List<Trafficker_victim> tv_List) {
		this.tv_List = tv_List;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ReportAction() {
		selectDivision();
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Report getReport() {
				return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public String getDivname() {
		return divname;
	}

	public void setDivname(String divname) {
		this.divname = divname;
	}
public String reportAgain(){
	report=dbo.updateReportStatus(id);
	return SUCCESS;			
}
	public List<Report> getReportlist() {
		return reportlist;
	}

	public void setReportlist(List<Report> reportlist) {
		this.reportlist = reportlist;
	}

	public List<Division> getDivlist() {
		return divlist;
	}

	public void setDivlist(List<Division> divlist) {
		this.divlist = divlist;
	}

	public Trafficker_victim getTv() {
		return tv;
	}

	public void setTv(Trafficker_victim tv) {
		this.tv = tv;
	}

	public String selectDivision() {
		divlist = dbo.retrieveDivisionFromReport();
		return SUCCESS;
	}
	
	public String selectallReports() {
		System.out.println(getDivname());
		reportlist = dbo.retrieveallreport();
		if (reportlist != null)
			return SUCCESS;
		else
			return ERROR;
	}
	
	public String selectReports() {
		System.out.println(getDivname());
		reportlist = dbo.retrievereport(divname);
		if (reportlist != null)
			return SUCCESS;
		else
			return ERROR;
	}
	public String recordCase(){
		
				report=dbo.retrieveOneReport(id);
				if(tv!=null)
				insertPeople();
		return SUCCESS;
	}
public String insertPeople(){
	System.out.println("inside insertPeople");
	
	if(tv!=null){	
		tv.setC(report.getC());
		dbo.addRespondentVictim(tv);
		System.out.println("Save Trafficker or Victim Success");
	}
	tv_List=dbo.retrieveAllPeople(id);
	return SUCCESS;
}
	public String selectOneReport(){
		System.out.println("Inside OneReport");
		
		report=dbo.retrieveOneReport(reportId);	
		return SUCCESS;
	}
	
	public String saveData(){
		System.out.println("In saveData");
		dbo.saveTV(tv);
			return SUCCESS;
		//else return ERROR;
	}
}
