package actions;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;


import dbservice.DBOperation;

import entities.Public_Information;

public class PublicInformationAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Public_Information p_info;
	private String date;
	private SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	private DBOperation dao=new DBOperation();
	Calendar cal = Calendar.getInstance();
	private MyFile upload;

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public MyFile getUpload() {
		return upload;
	}
	public void setUpload(MyFile upload) {
		this.upload = upload;
	}
	
	public Public_Information getP_info() {
		return p_info;
	}
	public void setP_info(Public_Information p_info) {
		this.p_info = p_info;
	}
	
	
	public String addPublicInfo() throws ParseException
	{		
		
		p_info.setPublic_date( sdf.format(cal.getTime()));
		p_info.setType("News");
		
		try{
			//create a new file within specified directory in which uploaded file will be copied
		String p=ServletActionContext.getServletContext().getRealPath("/");
		p="C:\\Apache Software Foundation\\Tomcat 7.0\\webapps\\Human -Trafficking\\Activity_Photo\\";
		System.out.println("New File path is"+p);		
			  if(upload!=null){
		    File file=new File(p+upload.getImageFileName());
		    System.out.println("Absolute Path is " + file.getAbsolutePath());
			//get the file uploaded by user
		    File uploadedFile = upload.getImage();		  
			//use commons IO to Copy the uploadedFile to the new file			
			FileUtils.copyFile(uploadedFile,file);
			p_info.setPath("/Human -Trafficking/Activity_Photo/"+file.getName());}
			System.out.println("Upload File Path is"+p_info.getPath());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			addActionError("Exception occured while creating file");
			return INPUT;
		}
		
		
		if(dao.insertInformation(p_info))
		{
			return SUCCESS;
		}
		
		else
		{
			return ERROR;
		}
	}
	
	public String addPublicLaw()
	{
		cal.getTime();
		p_info.setPublic_date(sdf.format( cal.getTime()));
		p_info.setType("Laws");
		
		try{
			//create a new file within specified directory in which uploaded file will be copied
		String p=ServletActionContext.getServletContext().getRealPath("/");
		p="C:\\Apache Software Foundation\\Tomcat 7.0\\webapps\\Human -Trafficking\\filedownload\\";
		System.out.println("New File path is"+p);		
			  if(upload!=null){
		    File file=new File(p+upload.getImageFileName());
		    System.out.println("Absolute Path is " + file.getAbsolutePath());
			//get the file uploaded by user
		    File uploadedFile = upload.getImage();		  
			//use commons IO to Copy the uploadedFile to the new file			
			FileUtils.copyFile(uploadedFile,file);
			p_info.setPath("/filedownload/"+file.getName());}
			System.out.println("Upload File Path is"+p_info.getPath());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			addActionError("Exception occured while creating file");
			return INPUT;
		}
		
		
		if(dao.insertInformation(p_info))
		{
			return SUCCESS;
		}
		
		else
		{
			return ERROR;
		}
	}
	

}
