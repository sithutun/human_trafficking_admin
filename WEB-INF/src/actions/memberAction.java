package actions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dbservice.DBOperation;
import entities.Case;
import entities.Division;
import entities.Instruction;
import entities.Member;

public class memberAction extends ActionSupport  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int case_id;
	private int id;
	private String idString;
	private Case c;
	private MyFile upload;
	private Instruction instruction;
	private Member member;
	private SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	private DBOperation dao=new DBOperation();
	private List<Member> memberList=new ArrayList<>();
	private List<Division> divlist=new ArrayList<>();
	public MyFile getUpload() {
		return upload;
	}


	public List<Member> getMemberList() {
		return memberList;
	}


	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}


	public List<Division> getDivlist() {
		return divlist;
	}


	public void setDivlist(List<Division> divlist) {
		this.divlist = divlist;
	}


	public void setUpload(MyFile upload) {
		this.upload = upload;
	}


	public int getCase_id() {
		return case_id;
	}


	public void setCase_id(int case_id) {
		this.case_id = case_id;
	}


	public Instruction getInstruction() {
		return instruction;
	}


	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}


	public Case getC() {
		return c;
	}


	public void setC(Case c) {
		this.c = c;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIdString() {
		return idString;
	}


	public void setIdString(String idString) {
		this.idString = idString;
	}


	public Member getMember() {
		return member;
	}


	public void setMember(Member member) {
		this.member = member;
	}

@Override
	public String execute() throws Exception {
	HttpSession session=ServletActionContext.getRequest().getSession();
  session.getAttribute("adminLogin");		
		return SUCCESS;
	}


	//Updating Member Detail
	public String updateMember(){
		try{
			System.out.println("Inside updating Member");
			//create a new file within specified directory in which uploaded file will be copied
		String p=ServletActionContext.getServletContext().getRealPath("/");
	System.out.println("New File path is"+p);		
			  if(upload!=null){
		    File file=new File(p+"\\member_Photo\\"+upload.getImageFileName());
			//get the file uploaded by user
		    File uploadedFile =upload.getImage(); 
		    	
			//use commons IO to Copy the uploadedFile to the new file			
			FileUtils.copyFile(uploadedFile,file);
			member.getPersonal_Detail().setPhoto(ServletActionContext.getServletContext().getContextPath()+"/member_Photo/"+file.getName());
			}
			System.out.println("Upload File Path is"+member.getPersonal_Detail().getPhoto());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			addActionError("Exception occured while creating file");
			return INPUT;
		}
		Member m=dao.retrieveMemberDetail(member.getMember_id());
		m.setPosition(member.getPosition());
		m.getPersonal_Detail().setAddress(member.getPersonal_Detail().getAddress());
		m.getPersonal_Detail().getAddress().setDiv(member.getDivision().getDivision_name());
		if(member.getPersonal_Detail().getPhoto()!=null)
		m.getPersonal_Detail().setPhoto(member.getPersonal_Detail().getPhoto());
		else
			member.getPersonal_Detail().setPhoto(m.getPersonal_Detail().getPhoto());
		if(dao.updateMember(m))		
		return SUCCESS;
		else return ERROR;
	}

	//Retrieve Case 
	public String send(){
		
		System.out.println("Inside Send Method \n Case ID is"+instruction.getC().getCase_id());
	Calendar	today=Calendar.getInstance();
	instruction.setSend_date(sdf.format(today.getTime()));
		instruction=dao.sendInstruction(instruction);
		if(instruction!=null)
		return SUCCESS;
		else return ERROR;
	}
	
	//Retrive Member Deatail
	public String memberDetail(){
		System.out.println("Inside Member Detail ID is " + id);
		member=dao.retrieveMemberDetail(id);
		System.out.println("ID is"+member.getMember_id()+member.getMember_name()+"\nPosition is" + member.getPosition());
		return SUCCESS;
	}

//Inserting New Member
	public String insertNewMember(){
		String statuString=SUCCESS;
		if(member!=null){
			if(member.getPosition()==null){
			addFieldError(member.getPosition(), "ရာထူး ဖြည့်စွက်ရန်");
			statuString=INPUT;
			}
		try{
			System.out.println("Inside Inserting New Member");
			//create a new file within specified directory in which uploaded file will be copied
		String p=ServletActionContext.getServletContext().getRealPath("/");
	System.out.println("New File path is"+p);		
			  if(upload!=null){
		    File file=new File(p+"\\member_Photo\\"+upload.getImageFileName());
			//get the file uploaded by user
		    File uploadedFile =upload.getImage(); 
		    	
			//use commons IO to Copy the uploadedFile to the new file			
			FileUtils.copyFile(uploadedFile,file);
			member.getPersonal_Detail().setPhoto(ServletActionContext.getServletContext().getContextPath()+"/member_Photo/"+file.getName());
			}
			System.out.println("Upload File Path is"+member.getPersonal_Detail().getPhoto());
			statuString=SUCCESS;
		}
		catch(Exception ex)
		{statuString=INPUT;
			ex.printStackTrace();
			addActionError("Exception occured while creating file");
			
		}
		member.getPersonal_Detail().getAddress().setDiv(member.getDivision().getDivision_name());
	
		if(dao.insertNewMember(member))
		statuString=SUCCESS;
		return statuString;
		}
					else return statuString;
	}
	//Deleting the Member
	public String delete(){
		
		divlist=dao.retrieveDivisionFromMember();
		System.out.println("Inside delete Member");
		if(dao.deleteMember(id)){
			memberList=dao.retrieveAllMembers();
		return SUCCESS;
		
		}
		else
		return	ERROR;
	}
	public String out(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		session.setAttribute("adminLogin", null);		
		return SUCCESS;
	}
}