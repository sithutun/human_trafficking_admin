<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="logoheader.ico" />

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<tiles:importAttribute name="masterCss" />
<tiles:importAttribute name="pageCss" />
<%-- <tiles:importAttribute name="displaytag"/> --%>
<!-- stylesheet information will be passed from our tiles.xml file, first stylesheet is for masterpage whereas second is for each individual page -->
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/${masterCss}" /> 
<%-- <link href="<%=request.getContextPath()%>/${displaytag}" rel="stylesheet" type="text/css" /> --%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/${pageCss}" /> 
 
<!-- This tag 's:head' is useful for displaying error messages in bold and red, also it will make (*) symbol on required field look red -->
<s:head />
</head>
<body>
<!-- We are going to provide layout information using stylesheet, hence arrange sections in 'div' -->
<!--This 'header' div will serve as the banner -->
<div id="container">
	<div id="logo">
		<tiles:insertAttribute name="logo"/>
	</div>
	<div id="header">
		<tiles:insertAttribute name="header" />
	</div>
<!--We will put together sidebar navigation menu, contentarea and footer in one div i.e. wrap, so that we can specify their position relative to 'wrap' -->
	<div id="wrap">
		<div id="menu">
			<tiles:insertAttribute name="menu" />
		</div>
		<div id="contentarea">
			<tiles:insertAttribute name="body" />
		</div>
		
	</div>
	<div id="footer">
				<tiles:insertAttribute name="footer" />
	</div>
	</div>
</body>
</html>
